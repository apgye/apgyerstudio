library(dplyr)
library(apgyeOperationsJusER)
library(apgyeDSL)
library(purrr)

primariasOrganismo <- function(organismoEXPR, desde, hasta) {
  organismoEXPR <- glob2rx(organismoEXPR)
  datos_operaciones <- list()

  operaciones <- apgyeJusEROrganization::listar_operaciones_por_organismo() %>%
    filter(grepl(organismoEXPR, organismo)) %>%
    .$operacion

  operaciones %>% lapply( function(op) {
      op <- rlang::enexpr(op)
      datosPrimarios <- function() {
        DB_PROD() %>% apgyeTableData(!! op) %>%
        filter(grepl(organismoEXPR, iep)) %>%
        apgyeDSL::interval(desde, hasta)
      }

      datos_operaciones[[op]] <<- list(
        ver = function() {
          datosPrimarios() %>% collect() %>% View()
        },
        obtener = function() {
          datosPrimarios() %>% collect()
        },
        tabla = function() {
          datosPrimarios()
        },
        procesar = function() {

          datosPrimarios() %>% apgyeDSL::process() %>% .$result
        }
      )
    }
  )

  datos_operaciones
}
