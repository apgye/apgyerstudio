---
title: "Causas a despacho"
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%Y-%m-%d')`"
params:
  aLaFecha: !r Sys.Date()
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(dplyr)
library(knitr)
library(kableExtra)
library(tibble)
library(lubridate)
```

## Causas a despacho por organismo 
### período : `r params$aLaFecha %m-% months(1) %>% format("%B %Y")`

```{r, warning=FALSE, render=FALSE}
adespacho <- apgyeRStudio::obtenerListadoADespacho(params$aLaFecha) %>% 
  arrange(circunscripcion, localidad, organismo_descripcion, fdesp) %>% 
  mutate(n = row_number()) %>% 
  group_by(circunscripcion, localidad, organismo_descripcion)
```


```{r}
adespachoConInfoGrupos <- adespacho
g <- group_indices(adespachoConInfoGrupos)
adespachoConInfoGrupos$g <- g
grupos <- adespachoConInfoGrupos %>% 
  group_by(g, circunscripcion, localidad, organismo_descripcion) %>% 
  summarise(min = min(n), max = max(n))

resultado <- adespachoConInfoGrupos %>% ungroup() %>% select("Nro" = nro, "Carátula" = caratula, "Fecha a despacho" = fdesp)%>%   
  kable() %>%
  kable_styling(c("striped", "bordered")) %>%
  column_spec(2, width = "7em")
  
for(i in 1:nrow(grupos)) {
  grupo <- grupos[i, ]
  resultado <- resultado %>% 
    group_rows(paste(grupo$circunscripcion, grupo$localidad, grupo$organismo_descripcion, sep=", "), grupo$min, grupo$max)
}  
resultado  

```
